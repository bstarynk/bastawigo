// file databasebw/database.go in gitlab.com/bstarynk/bastawigo.git 

/**
    BASTAWIGO - a blog engine in Go

    Copyright © 2018 Basile Starynkevitch  <basile@starynkevitch.net>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**/
package databasebw

import (
	"database/sql"
	"github.com/mattn/go-sqlite3"
	"log"
	"gitlab.com/bstarynk/bastawigo/configuration"
)

var db  *sql.DB


func CloseDatabase() {
	if (db != nil) {
		myconf := configuration.GetConfig()
		dbpath := myconf.General.Sqlite
		err := db.Close()
		if err != nil {
			log.Panicf("failed to close database: %s\n",
				err.Error());
		}
		log.Printf("bastawigo closed sqlite database %q\n", dbpath)
		db = nil
	}
}

func init_database() {
	initstmt := `
CREATE TABLE IF NOT EXISTS tbl_user (
  user_id INTEGER NOT NULL PRIMARY KEY,
  user_name VARCHAR(63) NOT NULL,
  user_email VARCHAR(63) NOT NULL,
  user_crypass TEXT NOT NULL,
  user_role CHAR(8) NOT NULL,
  user_mtime INTEGER NOT NULL
);
`;
	_, err := db.Exec(initstmt)
	if err != nil {
		log.Panicf("bastawigo failed to initialize database: %s\n%s\n",
		err.Error(), initstmt);
	}
}

func OpenDatabase() {
	myconf := configuration.GetConfig()
	dbpath := myconf.General.Sqlite
	mydb, myerr := sql.Open("sqlite3", dbpath)
	if myerr != nil {
		log.Panicf("bastawigo failed to open %q sqlite database : %s\n",
			dbpath, myerr.Error());
		return
	} else {
		libver, libnum, libsrc := sqlite3.Version()
		log.Printf("bastawigo opened %q sqlite database (v.%s n#%d s.%s)\n",
			dbpath, libver, libnum, libsrc);
	}
	db = mydb
	init_database()
	log.Printf("bastawigo using %q sqlite database\n", dbpath)
}
