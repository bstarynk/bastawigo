// file eventbw/event.go in gitlab.com/bstarynk/bastawigo.git 

/**
    BASTAWIGO - a blog engine in Go

    Copyright © 2018 Basile Starynkevitch  <basile@starynkevitch.net>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**/
package eventbw

import "log"

type Event interface {
	IsEvent() 
} 


type EventErrSt struct {
	Ev *Event
	Err error
}
var evchan  chan EventErrSt

var runloop bool

func init() {
	evchan = make( chan EventErrSt)
	runloop = true
}

func SendEventErr (ev *Event, err error) {
	log.Printf("SendEventErr ev=%#v err=%#v\n", ev, err);
	evchan <- EventErrSt {Ev: ev, Err: err}
}

func EventLoop() {
	var evcnt = 0;
	for runloop {
		evs := <- evchan
		evcnt++
		log.Printf("EventLoop#%d evs=%#v\n", evs);
	}
}
