# bastawigo

A simple wiki-like engine in Go

## build instructions

We need a `Makefile` to generate the timestamp, hence the entire program.
See https://stackoverflow.com/q/53031035/841108


## rich text editing
https://quilljs.com/ is interesting, it supports math formulas with https://katex.org/
http://www.wymeditor.org/ could also be interesting.

See also
https://techwiser.com/best-wysiwyg-html-editor-open-source/

We need an open source rich text editor. We don't care about old IE support: blog writers will have a recent Firefox or Chrome browser.

We do care about generating HTML5 conformant content.

## contact
Contact me Basile Starynkevitch by email <basile@starynkevitch.net>
See my web page http://starynkevitch.net/Basile/ for more about me.

Basile Starynkevitch; 92340 Bourg La Reine - France
