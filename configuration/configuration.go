// file configuration/configuration.go in gitlab.com/bstarynk/bastawigo.git 

/**
    BASTAWIGO - a blog engine in Go

    Copyright © 2018 Basile Starynkevitch  <basile@starynkevitch.net>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**/
package configuration
import "os"
import "io"
import "os/exec"
import "gopkg.in/gcfg.v1"

// this is a constant struct; don't change values in it!
type Configuration struct {
	General struct {
		Name string
		Format string
		RootTemplate string
		Sqlite string
	}
	Smtp struct {
		Identity string
		UserName string
		Passwd string
		Host string
		From string
	}
	Quill struct {
		Css string
		Script string
	}
	Http struct {
		Service string
	}
	Https struct {
		Service string
		CertificateFile string
		KeyFile string
	}
	Fcgi struct {
		Service string
	}
}

var curconfig Configuration 

func init () {
	curconfig.General.Name = "*builtin-configuration*"
	curconfig.Http.Service = ":8085"
}

func GetConfig() *Configuration {
	return &curconfig
}

func LoadConfigFromFile(filepath string) error {
	err := gcfg.ReadFileInto(&curconfig, filepath)
	if err == nil {
		if (len(curconfig.General.Name) == 0) {
			curconfig.General.Name = filepath
		}
	}
	return err
}

func LoadConfigFromPopen(command string) error {
	var cmdstr  string
	if (len(command)>2 && (command[0] == '|' || command[0] == '!')) {
		cmdstr = command[1:]
	} else {
		cmdstr = command
	}
	outstr, errc := exec.Command("/bin/sh", "-c", cmdstr).Output()
	if errc != nil {
		return errc;
	}
	err := gcfg.ReadStringInto(&curconfig, string(outstr))
	if err == nil {
		if (len(curconfig.General.Name) == 0) {
			curconfig.General.Name = command
		}
	}
	return err
}

func LoadConfigFromReader(rd io.Reader, confname string) error {
	err := gcfg.ReadInto(&curconfig, rd)
	if err == nil {
		if (len(curconfig.General.Name) == 0) {
			curconfig.General.Name = confname
		}
	}
	return err
}

func LoadConfigFromStdin() error {
	err := gcfg.ReadInto(&curconfig, os.Stdin)
	if err == nil {
		if (len(curconfig.General.Name) == 0) {
			curconfig.General.Name = "*stdin*"
		}
	}
	return err
}
