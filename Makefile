# file Makefile in gitlab.com/bstarynk/bastawigo.git

##   BASTAWIGO - a blog engine in Go

##   Copyright © 2018 Basile Starynkevitch  <basile@starynkevitch.net>
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.

##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.

##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.

.PHONY: all clean indent

GO= go
RM= rm -vf
MV= mv -vf
# conventionally, generated files end with _
GO_GENERATED_FILES := timestamp/timestamp_.go
GO_SOURCE_FILES := $(wildcard [a-z]*.go)
all: bastawigo

bastawigo: $(GO_SOURCE_FILES) $(GO_GENERATED_FILES) | go.mod
	$(GO) build -v
	$(MV) timestamp/timestamp_.go timestamp/timestamp_.go~

timestamp/timestamp_.go:
	echo "// Code generated - file" $@ "- DO NOT EDIT" > $@.tmp
	echo "package timestamp" >> $@.tmp
	date +' const TimeStamp = "%c %Z";%n const TimeLong = %s;%n' >> $@.tmp
	printf  ' const GitCommit = "%s";\n'  $$(git rev-parse HEAD) >> $@.tmp
	(echo -n ' const GitLastLog = "'; git log --format=oneline --abbrev=12 --abbrev-commit -q -1 | tr -d '\n\r\f\"\\\\'; echo '";') >> $@.tmp
	mv $@.tmp $@

indent: all
	$(MV) timestamp/timestamp_.go~ timestamp/timestamp_.go
	$(GO) fmt
	$(MV) timestamp/timestamp_.go timestamp/timestamp_.go~

clean:
	$(RM) bastawigo $(GO_GENERATED_FILES)
	$(RM) *~
	$(RM) *.tmp

