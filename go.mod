// file go.mod -*- indented-text -*-
module gitlab.com/bstarynk/bastawigo

require (
	github.com/mattn/go-sqlite3 v1.10.0
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
