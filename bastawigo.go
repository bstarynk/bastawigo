// file bastawigo.go in gitlab.com/bstarynk/bastawigo.git

/**
    BASTAWIGO - a blog engine in Go

    Copyright © 2018 Basile Starynkevitch  <basile@starynkevitch.net>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**/

package main

//import "fmt"
import "flag"
import "log"
import "os"
import "time"
import "net/http"
import "html/template"

import "gitlab.com/bstarynk/bastawigo/timestamp"
import "gitlab.com/bstarynk/bastawigo/configuration"
import "gitlab.com/bstarynk/bastawigo/eventbw"
import "gitlab.com/bstarynk/bastawigo/databasebw"

var configpath = flag.String("config", "bastawigo.conf", "configuration file")
var batchrun bool

var roottemplate *template.Template

func parseconfig() {
	var config string
	if configpath == nil {
		log.Printf("bastawigo no -config given\n")
	} else {
		config = *configpath
		log.Printf("bastawigo with config %q\n", config)
	}
	if (config[0] == '|' || config[0] == '!') &&
		len(config) >= 2 {
		log.Printf("loading configuration from command %s\n", config)
		err := configuration.LoadConfigFromPopen(config)
		if err != nil {
			log.Panicf("failed to load configuration from command %q : %v", config, err)
		}
	} else if config == "-" {
		log.Printf("loading configuration from stdin\n")
		err := configuration.LoadConfigFromStdin()
		if err != nil {
			log.Panicf("failed to load configuration from stdin : %s\n", err.Error())
		}
	} else {
		log.Printf("loading configuration from file %q\n", config)
		err := configuration.LoadConfigFromFile(config)
		if err != nil {
			log.Panicf("failed to load configuration from file %q : %s\n", config, err.Error())
		}
	}
}

func roothttphandler(rw http.ResponseWriter, req *http.Request) {
	type RootData struct {
		PageTitle string
		Role      string
		TimeStamp string
	}
	log.Printf("roothttphandler rw=%v req=%+#v\n.. roottemplate=%#v\n", rw, req, roottemplate)
	nowstr := time.Now().Local().Format(time.RFC1123)
	err := roottemplate.Execute(rw,
		RootData{"the title", "root", nowstr})
	if err != nil {
		log.Printf("roothttphandler error %s\n", err.Error())
	}
}

func servehttp(servnam string) {
	http.HandleFunc("/", roothttphandler)
	http.ListenAndServe(servnam, nil)
}

func main() {
	var hostname string = "?unknown-host?"
	hostname, _ = os.Hostname()
	cwd, _ := os.Getwd()
	log.Printf("Starting bastawigo pid %d on host %s built at %s in %q\n",
		os.Getpid(), hostname,
		timestamp.TimeStamp,
		cwd)
	log.Printf("bastawigo git last log %s\n", timestamp.GitLastLog)
	flag.BoolVar(&batchrun, "batch", false, "run in batch without event loop")
	flag.Parse()
	parseconfig()
	myconf := configuration.GetConfig()
	log.Printf("bastawigo using configuration %q:\n%+#v\n",
		myconf.General.Name,
		myconf)
	{
		tmplroot, tmplerr := template.ParseFiles(myconf.General.RootTemplate)
		if tmplerr != nil {
			log.Panicf("failed to parse root template %q: %v",
				myconf.General.RootTemplate, tmplerr)
		}
		roottemplate = tmplroot
	}

	if len(myconf.Http.Service) > 0 {
		log.Printf("bastawigo serving HTTP on %q\n",
			myconf.Http.Service)
		go servehttp(myconf.Http.Service)
	}
	//
	databasebw.OpenDatabase()
	defer databasebw.CloseDatabase()
	//
	if batchrun {
		log.Printf("bastawigo don't run the event loop in batch mode\n")
	} else {
		eventbw.EventLoop()
	}
	//
	log.Printf("Ending bastawigo pid %d on host %s\n",
		os.Getpid(), hostname)
}
